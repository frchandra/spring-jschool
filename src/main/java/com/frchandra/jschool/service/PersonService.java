package com.frchandra.jschool.service;

import com.frchandra.jschool.constants.JschoolConstants;
import com.frchandra.jschool.model.Person;
import com.frchandra.jschool.model.Roles;
import com.frchandra.jschool.repository.PersonRepository;
import com.frchandra.jschool.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RolesRepository rolesRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public boolean createNewPerson(Person person){
        boolean isSaved = false;
        Roles role = rolesRepository.getByRoleName(JschoolConstants.STUDENT_ROLE);
        person.setRoles(role);
        person.setPwd(passwordEncoder.encode(person.getPwd()));
        person = personRepository.save(person);
        if (null != person && person.getPersonId() > 0){
            isSaved = true;
        }
        return isSaved;
    }
}
