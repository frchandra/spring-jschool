package com.frchandra.jschool.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class ProjectSecurityConfig {
    /**
     * From Spring Security 5.7, the WebSecurityConfigurerAdapter is deprecated to encourage users
     * to move towards a component-based security configuration. It is recommended to create a bean
     * of type SecurityFilterChain for security related configurations.
     * @param http
     * @return SecurityFilterChain
     * @throws Exception
     */
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        //TODO: Learn more about this and middleware
        http.authorizeHttpRequests(auth -> auth
            .mvcMatchers( "/dashboard", "/displayProfile", "/updateProfile", "/api/**").authenticated()
            .mvcMatchers("/displayMessages", "/closeMsg", "/admin/**").hasRole("ADMIN")
            .mvcMatchers("/student/**").hasRole("STUDENT")
            .mvcMatchers("/","/home","/about","/login","/public/**", "/holidays/**","/courses","/contact", "/saveMsg").permitAll())

            .csrf()
                .ignoringAntMatchers("/saveMsg")
                .ignoringAntMatchers("/public/**")
                .ignoringAntMatchers("/api/**")
            .and()

            .httpBasic(Customizer.withDefaults())

            .formLogin(form -> form
                .loginPage("/login")
                .defaultSuccessUrl("/dashboard").failureUrl("/login?error=true").permitAll())

            .logout(logout -> logout
                .logoutSuccessUrl("/login?logout=true")
                .invalidateHttpSession(true)
                .permitAll());

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
