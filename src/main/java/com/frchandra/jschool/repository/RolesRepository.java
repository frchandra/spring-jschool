package com.frchandra.jschool.repository;

import com.frchandra.jschool.model.Contact;
import com.frchandra.jschool.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface RolesRepository extends JpaRepository<Roles, Integer>{
    Roles getByRoleName(String roleName);
}
