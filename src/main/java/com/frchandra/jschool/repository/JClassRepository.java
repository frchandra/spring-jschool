package com.frchandra.jschool.repository;

import com.frchandra.jschool.model.JClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JClassRepository extends JpaRepository<JClass, Integer> {

}
