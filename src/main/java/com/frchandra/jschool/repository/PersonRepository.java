package com.frchandra.jschool.repository;

import com.frchandra.jschool.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person readByEmail(String email);


}
