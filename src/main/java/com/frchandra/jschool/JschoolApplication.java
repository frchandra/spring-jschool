package com.frchandra.jschool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.frchandra.jschool.repository")
@EntityScan("com.frchandra.jschool.model")
@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl")
public class JschoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(JschoolApplication.class, args);

    }

}
